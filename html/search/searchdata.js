var indexSectionsWithContent =
{
  0: "abcdeghimnoqrstuv",
  1: "acdeimqst",
  2: "n",
  3: "acdegimqst",
  4: "abcdeghimnoqrstuv",
  5: "cdis",
  6: "i",
  7: "i"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "properties",
  7: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Properties",
  7: "Events"
};

